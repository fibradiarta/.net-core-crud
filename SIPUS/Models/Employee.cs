﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class Employee : BaseModels
    {
        [StringLength(9)]
        public string Gender { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(32)]
        public string Password { get; set; }
        [StringLength(50)]
        public string Position { get; set; }
        public int Shift { get; set; }
        [StringLength(12)]
        public string Phone { get; set; }
        [StringLength(255)]
        public string Address { get; set; }

        public ICollection<LoanTransaction> LoanTransactions { get; set; }
    }
}
