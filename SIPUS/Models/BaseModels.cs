﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class BaseModels
    {
        
        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }

        [StringLength(50)]
        public string UpadteBy { get; set; }

        public bool isActive { get; set; }
    }
}
