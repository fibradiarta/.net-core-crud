﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class Member : BaseModels
    {
        [StringLength(9)]
        public string Gender { get; set; }
        [StringLength(50)]
        public string Majors { get; set; }
        [StringLength(50)]
        public string Faculty { get; set; }
        [StringLength(12)]
        public string Phone { get; set; }
        [StringLength(255)]
        public string Address { get; set; }

        public ICollection<LoanTransaction> LoanTransactions { get; set; }

    }
}
