﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class BaseContext :DbContext
    {
        public BaseContext() { }

        public BaseContext(DbContextOptions<BaseContext> options) : base(options) { }

        public DbSet<Books> Books { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<LoanTransaction> LoanTransactions { get; set; }
        public DbSet<ReturnTransaction> ReturnTransactions { get; set; }

    }
}
