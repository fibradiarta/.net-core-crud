﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class ReturnTransaction
    {
        [Key]
        public int ReturnID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalDays { get; set; }
        public int TotalFine { get; set; }

        public LoanTransaction LoanTransaction { get; set; }
        public Employee Employee { get; set; }
    }
}
