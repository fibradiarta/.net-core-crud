﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class Books : BaseModels
    {
        public Books(){} 

        [StringLength(50)]
        public string Author { get; set; }
        [StringLength(100)]
        public string Publisher { get; set; }
        public DateTime PublicationYear { get; set; }
        public int RakNumber { get; set; }

        public ICollection<LoanTransaction> LoanTransactions { get; set; }
    }
}
