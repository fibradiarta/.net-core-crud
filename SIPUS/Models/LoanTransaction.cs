﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPUS.Models
{
    public class LoanTransaction
    {
        [Key]
        public int LoadID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Employee Employee { get; set; }
        public Member Member { get; set; }
        public Books Books { get; set; }

        public ICollection<ReturnTransaction> ReturnTransactions { get; set; }
    }
}
