﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIPUS.Models;

namespace SIPUS.Controllers
{
    public class BooksController : Controller
    {
        //BaseContext db = new BaseContext();

        private readonly BaseContext _context;

        public BooksController(BaseContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetDataBooks()
        {
            var book = _context.Books.Where(x=>x.isActive == true).ToList();
            

            return Json(new { data = book });
        }

        [HttpPost]
        public IActionResult Create([FromBody]Books books)
        {
            var message = "";
            try
            {
                books.CreateDate = DateTime.UtcNow;
                books.isActive = true;
                _context.Books.Add(books);
                _context.SaveChanges();
                message = "Sukses";

                return Json(message);
            }
            catch (Exception ex)
            {
                message = "Error";
                return Json(message);
            }
        }

        public IActionResult GetById(int Id)
        {
            return Json(new { data = _context.Books.Find(Id) });
        }

        public IActionResult Update([FromBody]Books books)
        {
            var message = "";
            try
            {
                books.isActive = true;
                books.UpdateDate = DateTime.UtcNow;
                _context.Entry(books).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
                message = "Update";

                return Json(message);
            }
            catch(Exception ex)
            {
                message = "Error";
                return Json(message);
            }
        }

        public IActionResult Delete([FromBody]Books books)
        {
            var message = "";
            try
            {
                var data = _context.Books.Find(books.ID);
                data.isActive = false;
                _context.Entry(data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
                message = "Delete";
                return Json(message);
            }
            catch
            {
                message = "Error";
                return Json(message);
            }
        }
    }
}