﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SIPUS.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    UpadteBy = table.Column<string>(maxLength: 50, nullable: true),
                    Author = table.Column<string>(maxLength: 50, nullable: true),
                    Publisher = table.Column<string>(maxLength: 100, nullable: true),
                    PublicationYear = table.Column<DateTime>(nullable: false),
                    RakNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    UpadteBy = table.Column<string>(maxLength: 50, nullable: true),
                    Gender = table.Column<string>(maxLength: 9, nullable: true),
                    Password = table.Column<string>(maxLength: 32, nullable: true),
                    Position = table.Column<string>(maxLength: 50, nullable: true),
                    Shift = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(maxLength: 12, nullable: true),
                    Address = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    UpadteBy = table.Column<string>(maxLength: 50, nullable: true),
                    Gender = table.Column<string>(maxLength: 9, nullable: true),
                    Majors = table.Column<string>(maxLength: 50, nullable: true),
                    Faculty = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 12, nullable: true),
                    Address = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "LoanTransactions",
                columns: table => new
                {
                    LoadID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EmployeeID = table.Column<int>(nullable: true),
                    MemberID = table.Column<int>(nullable: true),
                    BooksID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanTransactions", x => x.LoadID);
                    table.ForeignKey(
                        name: "FK_LoanTransactions_Books_BooksID",
                        column: x => x.BooksID,
                        principalTable: "Books",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LoanTransactions_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LoanTransactions_Members_MemberID",
                        column: x => x.MemberID,
                        principalTable: "Members",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReturnTransactions",
                columns: table => new
                {
                    ReturnID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    TotalDays = table.Column<int>(nullable: false),
                    TotalFine = table.Column<int>(nullable: false),
                    LoanTransactionLoadID = table.Column<int>(nullable: true),
                    EmployeeID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnTransactions", x => x.ReturnID);
                    table.ForeignKey(
                        name: "FK_ReturnTransactions_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReturnTransactions_LoanTransactions_LoanTransactionLoadID",
                        column: x => x.LoanTransactionLoadID,
                        principalTable: "LoanTransactions",
                        principalColumn: "LoadID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoanTransactions_BooksID",
                table: "LoanTransactions",
                column: "BooksID");

            migrationBuilder.CreateIndex(
                name: "IX_LoanTransactions_EmployeeID",
                table: "LoanTransactions",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_LoanTransactions_MemberID",
                table: "LoanTransactions",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnTransactions_EmployeeID",
                table: "ReturnTransactions",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnTransactions_LoanTransactionLoadID",
                table: "ReturnTransactions",
                column: "LoanTransactionLoadID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReturnTransactions");

            migrationBuilder.DropTable(
                name: "LoanTransactions");

            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Members");
        }
    }
}
